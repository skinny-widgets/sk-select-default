
import { SkSelectImpl }  from '../../sk-select/src/impl/sk-select-impl.js';

export class DefaultSkSelect extends SkSelectImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'select';
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }

    get selectEl() {
        if (! this._selectEl) {
            this._selectEl = this.comp.el.querySelector('select');
        }
        return this._selectEl;
    }

    set selectEl(el) {
        this._selectEl = el;
    }

    get subEls() {
        return [ 'selectEl' ];
    }

    onChange(event) {
        this.comp.callPluginHook('onEventStart', event);
        let selected = [];
        for (let option of this.body.selectedOptions) {
            selected.push(option.value || option.innerHTML);
        }
        this.comp.selectedValues = selected;
        this.comp.callPluginHook('onEventEnd', event);
    }

    restoreState(state) {
        super.restoreState(state);
        if (this.comp.hasAttribute('multiple')) {
            this.selectEl.setAttribute('multiple', '');
            for (let option of this.comp.el.querySelectorAll('option')) {
                let value = option.value || option.innerHTML;
                if (this.comp.isSelected(value)) {
                    option.setAttribute('selected', 'selected');
                }
            }
            this.selectEl.onchange = function(event) {
                this.onChange(event);
            }.bind(this);
        }
    }

    selectOption(option) {
        let i = -1;
        if (option && option.value) {
            for (let optionEl of this.body.querySelectorAll('option')) {
                i++;
                if (option.value === optionEl.value) {
                    optionEl.setAttribute('selected', 'selected');
                    this.selectEl.selectedIndex = i;
                } else {
                    if (option.hasAttribute('selected')) {
                        optionEl.removeAttribute('selected');
                    }
                }
            }
        } else {
            this.body.querySelectorAll('option').forEach((option) => {
                optionEl.removeAttribute('selected');
            });
            this.selectEl.selectedIndex = -1;
        }
    }

    enable() {
        super.enable();
        this.selectEl.removeAttribute('disabled');
    }

    disable() {
        super.disable();
        this.selectEl.setAttribute('disabled', 'disabled');
    }

    showInvalid() {
        super.showInvalid();
        this.selectEl.classList.add('form-invalid');
    }

    showValid() {
        super.showValid();
        this.selectEl.classList.remove('form-invalid');
    }

    focus() {
        this.selectEl.focus();
    }
}
