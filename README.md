# Skinny Widgets Select for Default Theme


select element

```
npm i sk-select sk-select-default --save
```

then add the following to your html

```html
<sk-config
    theme="default"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-default"
    lang="ru"
    id="skConfig"
></sk-config>
<sk-select id="skSelect">
    <option value="default">default</option>
    <option value="antd">antd</option>
    <option value="jquery">jquery</option>
</sk-select>
<script type="module">
    import { SkConfig } from '/node_modules/sk-config/src/sk-config.js';
    import { SkSelect } from '/node_modules/sk-select/src/sk-select.js';

    customElements.define('sk-config', SkConfig);
    customElements.define('sk-select', SkSelect);

    skSelect.value = skConfig.getAttribute('theme');
    skSelect.addEventListener('change', (event) => {
        skConfig.setAttribute('theme', event.target.value);
    }, false);
</script>
```

#### attributes

**multiple** - sk-select draws multiple selection widget and represent selected options as comma-separated list.

**value-type** - for multiselect **native** - show only first selected value as native elements, selectOptions prop will 
give you all of them, **default** -- comma-separated list of values. 

#### slots

**label** - draws label for select

#### template

id: SkSelectTpl